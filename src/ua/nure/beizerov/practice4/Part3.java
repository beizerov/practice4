package ua.nure.beizerov.practice4;


import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class is represent a Part3.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part3 {

	public static void main(String[] args) {	
		String input = Util.readFile("part3.txt");
		
		Pattern pattern;
		Matcher matcher;
		
		StringBuilder output = new StringBuilder();
		String regex;

		Command command;
		boolean isFind;
		
		try (Scanner scanner = new Scanner(System.in)) {	
			while (scanner.hasNext()) {
				try {
					command = Command.getCommandByName(scanner.nextLine());
					regex = getPatternByCommand(command);
					pattern = Pattern.compile(regex);
					
					if (Command.STOP.name().equalsIgnoreCase(regex)) {
						break;
					}
					
					matcher = pattern.matcher(input);
		
					isFind = false;
					while (matcher.find()) {
						isFind = true;
						output
							.append(matcher.group())
							.append(' ')
						;
					}
					
					if (isFind) {
						output
							.deleteCharAt(output.length() - 1)
							.append(System.lineSeparator())
						;
					} else {			
						output
							.append("No such values")
							.append(System.lineSeparator())
						;
					}
				} catch (IllegalArgumentException illegalArgumentException) {
					output
						.append("Incorrect input")
						.append(System.lineSeparator())
					;
				}
			}
		}
		
		if (output.length() != 0) {  
			Util.printOutput(
					output
						.deleteCharAt(output.length() - 1)
						.toString()
			);
		} else {
			Util.printOutput(output.toString());
		}
	}

	private enum Command {
		STRING("\\b\\p{L}{2,}\\b"),
		CHAR("^\\p{L}(?=\\s)|(?<=\\s)\\p{L}(?=\\s)"),
		INT("(?<=\\s)\\d+(?=\\s)"), 
		DOUBLE("\\d*\\.\\d*"), 
		STOP("stop");
		
		private final String pattern;
		
		
		Command(String pattern) {
			this.pattern = pattern;
		}
		
		
		private static Command getCommandByName(String command) {
			return Command.valueOf(command.toUpperCase(Locale.ENGLISH));
		}
	}
	
	private static String getPatternByCommand(Command command) {
		switch (command) {
			case STRING: 
				return Command.STRING.pattern;
			case CHAR:
				return Command.CHAR.pattern;
			case INT:
				return Command.INT.pattern;
			case DOUBLE:
				return Command.DOUBLE.pattern;
			default:
				return Command.STOP.pattern;
		}
	}
}
