package ua.nure.beizerov.practice4;


import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class is represent a Part4.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part4 {

	private static final String PATH = "part4.txt";
	private static final String REGEX = "(\\p{Lu}[^\\.]+\\s+\\p{L}+)+[\\.!?]";
	
	private final String input = Util.readFile(PATH);
	
	private final Pattern pattern = Pattern.compile(REGEX);
	private final Matcher matcher = pattern.matcher(
			input
				.replaceAll("\\s+", " ")
			);

	public static void main(String[] args) {
		StringBuilder output = new StringBuilder();
		
		for (Iterator<String> iterator = new Part4().iterator()
			; iterator.hasNext()
			;
		) {
			output
				.append(iterator.next())
				.append(System.lineSeparator());
		}
		
		Util.printOutput(output.deleteCharAt(output.length() - 1).toString());
	}

	
	public Iterator<String> iterator() {
		return new IteratorIml();
	}
	
	private final class IteratorIml implements Iterator<String> {
		
		private boolean isNext;
		
		
		@Override
		public boolean hasNext() {
			isNext = matcher.find();
			
			return isNext;
		}

		@Override
		public String next() {
			if(isNext) {
				isNext = false;
				
				return matcher.group();
			}
			
			throw new NoSuchElementException();	
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}
