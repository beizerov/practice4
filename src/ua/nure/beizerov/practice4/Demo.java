package ua.nure.beizerov.practice4;


import java.io.ByteArrayInputStream;
import java.io.InputStream;


/**
 * This class is represent a demo.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Demo {

	private static final InputStream STD_IN = System.in;

	public static void main(String[] args) {
		System.out.println("=========================== PART1");
		Part1.main(null);

		
		System.out.println("=========================== PART2");
		Part2.main(null);

		
		System.out.println("=========================== PART3");
		// set the mock input
		System.setIn(
			new ByteArrayInputStream(
				"double^char^String^double^int^stop"
					.replace("^", System.lineSeparator())
					.getBytes()
			)
		);
		Part3.main(null);
		// restore the standard input
		System.setIn(STD_IN);
		
		System.out.println("=========================== PART4");
        Part4.main(null);
        
        System.out.println("=========================== PART5");
        // set the mock input
        System.setIn(
        	new ByteArrayInputStream(
                "apple fr^apple en^asdf^table ru^stop"
        			.replace("^", System.lineSeparator())
        			.getBytes()
        	)
        );
        Part5.main(null);
        // restore the standard input
        System.setIn(STD_IN);
        
	    System.out.println("=========================== PART6");
        // set the mock input
        System.setIn(new ByteArrayInputStream(
                "asdf^Latn^Cyrl^latn^stop"
        			.replace("^", System.lineSeparator())
        			.getBytes()));
        Part6.main(args);
        // restore the standard input
        System.setIn(STD_IN);
	}
}
