package ua.nure.beizerov.practice4;


import java.io.File;
import java.io.FileNotFoundException;
import java.security.SecureRandom;
import java.util.Scanner;


/**
 * This class is represent a Part2.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part2 {

	private static final int SEED_FOR_RANDOM = 51;
	private static final String DATA_PATH = "part2.txt";
	private static final String PATH_TO_SORTED_DATA = "part2_sorted.txt";
	private static final int QUANTITY = 10;
	
	
	public static void main(String[] args) {
		StringBuilder sbRandomNumbers = new StringBuilder();
		
		for (int i = 0; i < QUANTITY; i++) {
			sbRandomNumbers
				.append(String.format(
							"%d", 
							new SecureRandom().nextInt(SEED_FOR_RANDOM)
						)
				)
				.append(" ");
		}
		sbRandomNumbers.deleteCharAt(sbRandomNumbers.length() - 1);
		
		Util.writeFile(DATA_PATH, sbRandomNumbers.toString());
		
		int[] numbers = new int[QUANTITY];
		
		try (Scanner scanner = new Scanner(new File(DATA_PATH))){
			
			int i = 0;
			
			while(scanner.hasNextInt()) {
				if (i == QUANTITY) {
					break;
				}
				
				numbers[i++] = scanner.nextInt();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		sortIntArray(numbers);
		
		StringBuilder sbNumbers = new StringBuilder();
		
		for (int i = 0; i < numbers.length; i++) {
			sbNumbers
				.append(numbers[i])
				.append(" ")
			;
		}
		sbNumbers.deleteCharAt(sbNumbers.length() - 1);
		
		Util.writeFile(PATH_TO_SORTED_DATA, sbNumbers.toString());
		
		StringBuilder output = new StringBuilder();
		
		output
			.append("input  ==> ")
			.append(Util.readFile(DATA_PATH))
			.append(System.lineSeparator())
			.append("output  ==> ")
			.append(Util.readFile(PATH_TO_SORTED_DATA))
		;
		
		Util.printOutput(output.toString());
	}
	
	
	private static void sortIntArray(int[] intArray) {
		int temporary;
		int i = 0;
		int k = 0;
		int j = 0;
		
		for (k = intArray.length / 2; k > 0; k /= 2) {
			for (i = k; i < intArray.length; i++) {
				temporary = intArray[i];
				
				for (j = i; j >= k; j -= k) {
					
					if (temporary < intArray[j - k]) {
						intArray[j] = intArray[j - k];
					} else {
						break;
					}
				}

				intArray[j] = temporary;
			}
		}
	}
}
