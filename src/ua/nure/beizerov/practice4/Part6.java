package ua.nure.beizerov.practice4;


import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class is represent a Part5.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part6 {

	public static void main(String[] args) {	
		String input = Util.readFile("part6.txt");
		
		Pattern pattern;
		Matcher matcher;
		
		StringBuilder output = new StringBuilder();
		String regex;
		String commandName;

		Command command;
		
		try (Scanner scanner = new Scanner(System.in)) {	
			while (scanner.hasNext()) {
				try {
					commandName = scanner.nextLine();
					
					if (Command.STOP.name().equalsIgnoreCase(commandName)) {
						break;
					}
					
					output
						.append(commandName)
						.append(": ")
					;
					
					command = Command.getCommandByName(commandName);
					regex = getPatternByCommand(command);
					
					pattern = Pattern.compile(regex);
					matcher = pattern.matcher(input);
					
					while (matcher.find()) {
						output
							.append(matcher.group())
							.append(' ')
						;
					}
					
					output
						.deleteCharAt(output.length() - 1)
						.append(System.lineSeparator())
					;
				} catch (IllegalArgumentException illegalArgumentException) {
					output
						.append("Incorrect input")
						.append(System.lineSeparator())
					;
				}
			}
		}

		Util.printOutput(output.deleteCharAt(output.length() - 1).toString());
	}
	
	private enum Command {
		LATN("\\b\\p{IsLatin}+\\b"),
		CYRL("\\b\\p{IsCyrillic}+\\b"),
		STOP("stop");
		
		private final String pattern;
		
		
		Command(String pattern) {
			this.pattern = pattern;
		}
		
		
		private static Command getCommandByName(String command) {
			return Command.valueOf(command.toUpperCase(Locale.ENGLISH));
		}
	}
	
	private static String getPatternByCommand(Command command) {
		switch (command) {
			case LATN: 
				return Command.LATN.pattern;
			case CYRL: 
				return Command.CYRL.pattern;
			case STOP:
				return Command.STOP.pattern;
			default:
				return Command.STOP.pattern;
		}
	}
}
