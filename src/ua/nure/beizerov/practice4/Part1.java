package ua.nure.beizerov.practice4;


import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class is represent a Part1.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part1 {

	public static void main(String[] args) {
		String input = Util.readFile("part1.txt");
		
		Pattern pattern = Pattern.compile("\\b[\\p{L}&&[^\\p{Lu}]]{4,}\\b");
		Matcher matcher = pattern.matcher(input);

		StringBuffer output = new StringBuffer();
		String word;

		while (matcher.find()) {
			word = matcher.group();
			matcher.appendReplacement(output, word.toUpperCase(Locale.ENGLISH));
		}

		matcher.appendTail(output);
		
		pattern = Pattern.compile("\\b(\\p{Lu})([\\p{L}&&[^\\p{Lu}]]{3,}\\b)");
		matcher = pattern.matcher(output.toString());
		output = new StringBuffer();
		String letter;
		
		while (matcher.find()) {
			letter = matcher.group(1);
			matcher.appendReplacement(
					output, 
					letter.toLowerCase(Locale.ENGLISH) 
					+ 
					matcher.group(2).toUpperCase(Locale.ENGLISH)
			);
		}

		matcher.appendTail(output);
		
		Util.printOutput(output.toString());
	}
}
