package ua.nure.beizerov.practice4;


import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;


/**
 * This class is represent a Part5.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part5 {
	
	private static final String BASE_NAME = "resources";

	public static void main(String[] args) {
		ResourceBundle rb;
		
		StringBuilder output = new StringBuilder();
		
		try (Scanner scanner = new Scanner(System.in)) {
			while (scanner.hasNext()) {
				Pair pair = Pair.create(scanner.nextLine().split(" "));
				
				if("stop".equals(pair.key)) {
					break;
				}
				
				try {
					if(!"stop".equals(pair.key) && pair.language == null) {
						throw new IllegalArgumentException();
					}
					
					rb = ResourceBundle.getBundle(
						BASE_NAME, new Locale(pair.language));
					
					output
						.append(rb.getString(pair.key))
						.append(System.lineSeparator())
					;
				} catch (IllegalArgumentException illegalArgumentException) {
					output
						.append("No such values")
						.append(System.lineSeparator())
					;
				}
			}
		}
		
		Util.printOutput(output.deleteCharAt(output.length() - 1).toString());
	}
	
	
	private static final class Pair {
		private final String key;
		private final String language;
		
		
		private Pair(String key, String language) {
			this.key = key;
			this.language = language;
		}

		
		private static Pair create(String[] args) {
			if (args != null) {
				switch(args.length) {
					case 1:
						return new Pair(args[0], null);	
					case 2:
						return new Pair(args[0], args[1]);
					default:
						break;
				}
			}
			return new Pair(null, null);
		}
	}
}
