package ua.nure.beizerov.practice4;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 * This class is represent a Utility.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public final class Util {

    private static final String ENCODING = "Cp1251";
    
    
    private Util() {
        throw new IllegalStateException("Util class");
    }
    
    
    public static String readFile(String path) {
        StringBuilder fileResource = new StringBuilder();
        
        try (BufferedReader reader = Files.newBufferedReader(
        		Paths.get(path), Charset.forName(ENCODING)
        	)) {
        	
        	String line = null;
            while ((line = reader.readLine()) != null) {
                fileResource
                	.append(line)
                	.append(System.lineSeparator());
            }
        } catch (IOException e) {
			e.printStackTrace();
		}
        
        return fileResource.toString();
    }
    
    public static void writeFile(String fileName, String data) {
    	try(PrintWriter printWriter = new PrintWriter(
    			new BufferedWriter(
    					new FileWriter(new File(fileName))))) {
    		printWriter.append(data);
    	} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	public static void printOutput(String output) {
		System.out.println(output);
	}
}
